<?php require_once('config.php'); ?>

<?php get_header(); ?>

<div id="single" class="page">

    <div id="top">
        <a class="<?php if (LOGO_FONT) { echo 'icon-icon'; } else { echo 'image-icon'; } ?>" href="javascript:history.back()"></a>
    </div>

    <div class="section">
	<div class="images">
	</div><div class="article">
        <div>

        <div class="content">
            <h1>404 Not Found</h1>
            <p>页面找不到了，搜索或者去首页看看.</p>
        </div>

        </div>
        </div>
    </div>

</div>

<?php get_footer(); ?>
